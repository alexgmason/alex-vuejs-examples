import Vue from 'vue'
import Router from 'vue-router'

import { isLoggedIn } from './helpers/auth'

// import Home from './views/Home.vue'
import About from './views/About.vue'
import Login from './views/Login.vue'
import Logout from './views/Logout.vue'
import Callback from './views/Callback.vue'
import Campaigns from './views/Campaigns.vue'
import Connections from './views/Connections.vue'
import Contents from './views/Contents.vue'
import Medias from './views/Medias.vue'
import Notifications from './views/Notifications.vue'
import AddNewCampaign from './components/AddNewCampaign.vue'
import AddSocial from './views/AddSocialContent.vue'
import EditSocial from './views/EditSocialContent.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'home',
    component: Campaigns
  },
  {
    path: '/about',
    name: 'about',
    component: About
  },
  {
    path: '/connections',
    name: 'connections',
    component: Connections
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout
  },
  {
    path: '/callback*',
    name: 'callback',
    component: Callback
  },
  {
    path: '/social',
    name: 'social',
    component: Contents,
    children: [{
      path: 'add',
      name: 'add-social',
      component: AddSocial
    },
    {
      path: '/social/:contentUuid',
      name: 'edit-social',
      component: EditSocial,
      props: true
    }]
  },
  {
    path: '/media',
    name: 'medias',
    component: Medias
  },
  {
    path: '/campaigns',
    name: 'campaigns',
    component: Campaigns,
    children: [{
      path: 'add',
      name: 'add-campaign',
      component: AddNewCampaign
    }]
  },
  {
    path: '/notifications',
    name: 'notifications',
    component: Notifications
  }]
})

router.beforeEach((to, from, next) => {
  if (to.name !== 'login' &&
    to.name !== 'callback' &&
    !isLoggedIn()) {
    next('/login')
  }
  next()
})

export default router
